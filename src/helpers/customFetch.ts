/* eslint-disable no-undef */
import { getToken } from './localStorage';

const customFetch = async(url:string, method: string, data?: any) => {
  const headers: any = {};

  // if (!url.includes('login') && !url.includes('forgot-password')) {
  //   if (!isUpload) {
  //   }
  // }

  // headers['Content-Type'] = 'application/json; charset=utf-8';
  // headers['Authorization'] = getToken();
  // headers['X-Channel'] = 'cms_channel';

  try {
    const response = await fetch(url, {
      method: method,
      body: method !== 'GET' ? JSON.stringify(data) : undefined,
      // mode: 'cors',
      headers,
    });

    switch (response.status) {
      case 404:
        window.location.replace('/404');
        break;

      case 403:
        // window.location.replace('/403');
        break;

      case 401:
        // if (!window.location.pathname.includes('performance-mc')) {
        //   localStorage.removeItem('token');
        //   localStorage.removeItem('userId');
        //   window.location.reload();
        // } else {
        //   window.location.replace('/401');
        // }
        break;

      case 400:
        // const res = await response.json();
        // if (res.stat_msg.toLowerCase() === 'invalid token') {
        //   localStorage.removeItem('token');
        //   localStorage.removeItem('userId');
        //   window.location.replace('/signin');
        // } else {
        //   return res;
        // }
        break;

      default:
        return await response;
    }
  } catch (err) {
    throw err;
  }
};
export default customFetch;
