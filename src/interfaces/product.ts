export interface State {
  list: Detail[],
  loadingGet: boolean
}

export interface Detail {
  id: string
  name: string
}

export interface APIDetail {
  id: number
  product_name: string;
}