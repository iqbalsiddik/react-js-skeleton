import { Product } from './index';

export interface ApiResponseData<T> {
  stat_code: number;
  stat_msg: string;
  pagination: Pagination;
  data: T;
}

export interface ApiParams {
  id?: string;
  page?: string;
  keyword?: string;
}

export interface Pagination {
  count?: number;
  keyword?: string;
  limit?: number;
  offset?: number;
  order?: string;
  page?: number;
  sort?: string;
}

export interface RootState {
  product: Product.State
}
export interface InputsProps {
  type?: string;
  placeholder?: string;
  name: string;
  label?: string;
  accept?: string;
  value?: any;
  formik?: any;
  readOnly?: boolean;
  handleUpload?: any;
  mask?: any;
  mask_name?: string;
  id?: string;
  currency_name?: string;
  isCurrency?: boolean;
}
