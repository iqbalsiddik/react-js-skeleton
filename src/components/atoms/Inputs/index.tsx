/* eslint-disable react/jsx-no-bind */
/* eslint-disable no-unused-vars */
import { useState } from 'react';
import { Common } from 'interfaces';
import { FiEye, FiEyeOff } from 'react-icons/fi';
import InputMask from 'react-input-mask';
import formatRupiah from 'helpers/formatRupiah';


export const Inputs = (props: Common.InputsProps) => {
  const {
    type,
    placeholder,
    name,
    label,
    value,
    formik,
    readOnly,
    accept,
    handleUpload,
    id,
  } = props;

  const [showPassword, setshowPassword] = useState(false);
  let tipe = type;
  if (type === 'passsword' || showPassword) {
    tipe = 'text';
  }
  
  const handleOnChange = (e: any) => {
    if (props.mask && props.mask_name) {
      props.formik.setFieldValue(props.mask_name, e.target.value);
      var find = '-|_';
      var re = new RegExp(find, 'g');
      props.formik.setFieldValue(props.name, e.target.value.replace(re, '').replace(' (', '').replace(') ', ''));
    } else if (props.isCurrency && props.currency_name) {
      props.formik.setFieldValue(props.currency_name, e.target.value);
    } else {
      props.formik.setFieldValue(props.name,
        tipe === 'file' ? e.target.files[0] : e.target.value
      );
  
      if (tipe === 'file' && handleUpload) {
        handleUpload(e.target.files[0]);
      }
    }
  };

  const handleOnKeyUp = e => {
    if (props.isCurrency && props.currency_name) {
      const format = formatRupiah(e.target.value);
      props.formik.setFieldValue(props.currency_name, format);
      const value = e.target.value.split('.').join('').replace('Rp ', '');
      props.formik.setFieldValue(props.name, value);
    }
  };

  return (
    <div>
      { label && (
        <label
          className='font-OpenSans text-[#333333] mb-1 text-sm'
        >
          { label }
        </label>
      ) }
      <div className='relative'>
        {
          props.mask ?
          <InputMask mask={ props.mask } placeholder={ placeholder } value={ value } onChange={ e => handleOnChange(e) } className={ `w-full !py-2 pl-3 pr-4 border rounded focus:outline-none focus:ring-blue-100 focus:ring-2 ${formik.errors[name] && formik.touched[name] && 'ring-2 ring-red-600'} ${readOnly && 'bg-gray-200'}` } />
          :
          <input
            id={ id }
            accept={ accept ?? '' }
            type={ tipe }
            className={ `w-full py-2 pl-3 pr-4 border rounded focus:outline-none focus:ring-blue-100 focus:ring-2 ${formik.errors[name] && formik.touched[name] && 'ring-2 ring-red-600'} ${readOnly && 'bg-gray-200'}` }
            placeholder={ placeholder }
            name={ name }
            value={ value }
            readOnly={ readOnly }
            onChange={ e => handleOnChange(e) }
            autoComplete='off'
            onKeyUp={ e => handleOnKeyUp(e) }
          />
        }
        {
          type === 'password' ? (
             <div>
               {
                showPassword ?
                  <FiEyeOff className='absolute top-[14px] right-2 cursor-pointer' onClick={ () => setshowPassword(!showPassword) } />
                :
                  <FiEye className='absolute top-[14px] right-2 cursor-pointer' onClick={ () => setshowPassword(!showPassword) } />
              }
             </div>
          ) :
          null
        }
        {
          formik.errors[name] && props.formik.touched[name] &&
          (<p className='text-red-600 text-sm mt-1' >{ formik.errors[name] }</p>)
        }
      </div>
    </div>
  );
};