import React from 'react';

import {StyledText} from './style';

/**
 * ==========================================================================================
 * @name Text
 * @param {object} props
 * @description component for rendering all text eg: h1- h6, p
 * ==========================================================================================
 */

const Text: React.FC<TextProps> = ({color, text, ...props}) => {
  return (
    <StyledText color={ color } {...props}>
      {text}
    </StyledText>
  );
};

type TextProps = {
  text?: string;
  color?: string;
};

export default Text;
