import { createSelector } from 'reselect';

import { Product } from 'interfaces';

const productSelector = (product: Product.State) => product.list;

const productListSelector = createSelector(productSelector, list => list);

export { productListSelector };
