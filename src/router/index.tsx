import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import { Dashboard } from 'screens';
import { GlobalStyles } from 'constant';
import PrivateRoute from './private';

const publicRoutes = [
  {
    path: '/',
    component: Dashboard,
  },
  {
    path: '/404',
    component: Dashboard,
  },
];

interface Props {
  // eslint-disable-next-line no-undef
  history: any;
}

const Router = (props: Props) => {
  return (
    <ConnectedRouter history={ props.history }>
      <GlobalStyles />
      <Switch>
        {
          publicRoutes.map(route =>
            <Route key={ route.path } exact path={ route.path } component={ route.component } />
          )
        }
        {/* <PrivateRoute path='/' component={ App } /> */}
      </Switch>
    </ConnectedRouter>
  );
};

export default Router;
