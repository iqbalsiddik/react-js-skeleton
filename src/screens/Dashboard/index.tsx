import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import * as productActions from 'store/product/actions';
import { productListSelector } from 'store/product/selector';
import { Common, Product } from 'interfaces';

type State = {
  example: boolean
};

const Dashboard = (props: RouteComponentProps<{id?:string}> & State) => {
  // MAPSTATE
  const products: Product.Detail[] = useSelector((state: Common.RootState) => productListSelector(state.product));

  // MAPDISPATCH
  const dispatch = useDispatch();
  const getProduct = dispatch(productActions.getProduct);

  const [setExample, example] = useState(true);

  useEffect(() => {
    getProduct({ page: '2' });
  }, []);

  return (
    <div className='flex justify-center items-center h-screen'>
      <div className=' text-black'>
        HALLO WORD
      </div>
    </div>
  );
};

export default Dashboard;
